---@type ChadrcConfig 
local M = {}
M.ui = {
  theme = 'gruvchad',
  hl_override = {
    Comment = { fg = "nord_blue"},
    LineNr = {fg = "teal"},
  }
}

M.plugins = 'custom.plugins'
return M
